<?php

namespace Tests\Feature\AutoGen\API\V1;

use App\Entities\Books\Book;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksShowBookAPITest extends APIBaseTestCase
{

	use DatabaseTransactions;

    /**
     *
     * Get Book Details
     *
     * @return  void
     */
    public function test_api_books_get_show_book()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        Book::truncate();
        Book::create([
            'name' => 'Book1',
            'author_name' => 'Otwell',
            'isbn_no' => 12626878,
            'published_year' => 2017
        ]);

					// header params
	        	     $headers['Accept'] = 'application/json';
	            	 $headers['x-access-token'] = $this->getAccessToken();
	                 $headers['x-api-key'] = $this->getApiKey();
	                    	        		
		
        $response = $this->get('/api/v1api/books/1', $headers);
                
        $this->saveResponse($response->getContent(), 'books_get_show_book', $response->getStatusCode());

		$response->assertStatus(200);
        $response->assertJson([
            'name' => 'Book1',
            'author_name' => 'Otwell',
            'isbn_no' => 12626878,
            'published_year' => 2017
        ]);
    }

}
