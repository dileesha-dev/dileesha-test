<?php

namespace Tests\Feature\AutoGen\API\V1;

use App\Entities\Books\Book;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksBookListAPITest extends APIBaseTestCase
{

	use DatabaseTransactions;

    /**
     *
     * Get Book List
     *
     * @return  void
     */
    public function test_api_books_get_book_list()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        Book::truncate();
        Book::create([
            'name' => 'Book1',
            'author_name' => 'Otwell',
            'isbn_no' => 12626878,
            'published_year' => 2017
        ]);
        Book::create([
            'name' => 'Book2',
            'author_name' => 'Martin',
            'isbn_no' => 32424234,
            'published_year' => 2014
        ]);

					// header params
                        $headers['Accept'] = 'application/json';
	            	    $headers['x-access-token'] = $this->getAccessToken();
	                    $headers['x-api-key'] = $this->getApiKey();
	                    	        		
		
        $response = $this->get('/api/v1api/books', $headers);
                
        $this->saveResponse($response->getContent(), 'books_get_book_list', $response->getStatusCode());

		$response->assertStatus(200);
		$response->assertJsonCount(2);
    }

}
