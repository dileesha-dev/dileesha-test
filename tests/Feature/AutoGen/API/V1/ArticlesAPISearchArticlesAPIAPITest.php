<?php

namespace Tests\Feature\AutoGen\API\V1;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticlesAPISearchArticlesAPIAPITest extends APIBaseTestCase
{

	use DatabaseTransactions;

    /**
     *
     * 
     *
     * @return  void
     */
    public function test_api_articlesapi_get_search_articlesapi()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

					// header params
	        	    $headers['Accept'] = 'application/json';
	            	$headers['x-access-token'] = $this->getAccessToken();
	                $headers['x-api-key'] = $this->getApiKey();
	                    	        		
		
                        $response = $this->get('/api/v1api/articles', $headers);
                
        $this->saveResponse($response->getContent(), 'articlesapi_get_search_articlesapi', $response->getStatusCode());

		$response->assertStatus(200);
    }

}
