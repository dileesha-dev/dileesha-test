define({ "api": [
  {
    "description": "<p>Get Book List</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/books",
    "title": "Book List",
    "group": "Books",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Book name</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "sort_order",
            "description": "<p><code>1</code> =&gt; Oldest First, <code>2</code> =&gt; Newest First</p>"
          }
        ]
      }
    },
    "filename": "resources/docs/apidoc/auto_generated/books.coffee",
    "groupTitle": "Books",
    "name": "GetApiBooks",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  },
  {
    "description": "<p>Get Book Details</p>",
    "version": "1.0.0",
    "type": "GET",
    "url": "api/books/{id}",
    "title": "Show Book",
    "group": "Books",
    "filename": "resources/docs/apidoc/auto_generated/books.coffee",
    "groupTitle": "Books",
    "name": "GetApiBooksId",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p>Set to <code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-api-key",
            "description": "<p>API Key</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>Unique user authentication token</p>"
          }
        ]
      }
    }
  }
] });
