{
    "swagger": "2.0",
    "info": {
        "title": "Workbench API",
        "version": "1.0.0.20210729"
    },
    "host": "workbench.test",
    "schemes": [
        "http"
    ],
    "basePath": "\/api\/v1",
    "paths": {
        "api\/books": {
            "get": {
                "tags": [
                    "Books"
                ],
                "summary": "Book List",
                "consumes": [
                    "application\/json",
                    "application\/x-www-form-urlencoded"
                ],
                "produces": [
                    "application\/json"
                ],
                "operationId": "books_get_book_list",
                "description": "Get Book List",
                "parameters": [
                    {
                        "name": "name",
                        "in": "query",
                        "required": true,
                        "description": "Book name",
                        "type": "string"
                    },
                    {
                        "name": "sort_order",
                        "in": "path",
                        "required": true,
                        "description": "`1` => Oldest First, `2` => Newest First",
                        "type": "integer"
                    },
                    {
                        "name": "Accept",
                        "in": "header",
                        "required": true,
                        "description": "Set to `application\/json`",
                        "type": "string",
                        "schema": {
                            "type": "string",
                            "example": "application\/json"
                        }
                    }
                ],
                "security": [
                    {
                        "apiKey": []
                    },
                    {
                        "accessToken": []
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#\/definitions\/BooksBookListResponse"
                        },
                        "description": "BooksBookListResponse"
                    },
                    "401": {
                        "schema": {
                            "$ref": "#\/definitions\/ApiErrorUnauthorized"
                        },
                        "description": "Authentication failed"
                    },
                    "403": {
                        "schema": {
                            "$ref": "#\/definitions\/ApiErrorAccessDenied"
                        },
                        "description": "Access denied"
                    },
                    "422": {
                        "schema": {
                            "$ref": "#\/definitions\/ApiError"
                        },
                        "description": "Generic API error. Check `message` for more information."
                    }
                }
            }
        },
        "api\/books\/{id}": {
            "get": {
                "tags": [
                    "Books"
                ],
                "summary": "Show Book",
                "consumes": [
                    "application\/json",
                    "application\/x-www-form-urlencoded"
                ],
                "produces": [
                    "application\/json"
                ],
                "operationId": "books_get_show_book",
                "description": "Get Book Details",
                "parameters": [
                    {
                        "name": "Accept",
                        "in": "header",
                        "required": true,
                        "description": "Set to `application\/json`",
                        "type": "string",
                        "schema": {
                            "type": "string",
                            "example": "application\/json"
                        }
                    }
                ],
                "security": [
                    {
                        "apiKey": []
                    },
                    {
                        "accessToken": []
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#\/definitions\/BooksShowBookResponse"
                        },
                        "description": "BooksShowBookResponse"
                    },
                    "401": {
                        "schema": {
                            "$ref": "#\/definitions\/ApiErrorUnauthorized"
                        },
                        "description": "Authentication failed"
                    },
                    "403": {
                        "schema": {
                            "$ref": "#\/definitions\/ApiErrorAccessDenied"
                        },
                        "description": "Access denied"
                    },
                    "422": {
                        "schema": {
                            "$ref": "#\/definitions\/ApiError"
                        },
                        "description": "Generic API error. Check `message` for more information."
                    }
                }
            }
        }
    },
    "securityDefinitions": {
        "apiKey": {
            "type": "apiKey",
            "name": "x-api-key",
            "in": "header",
            "description": "API Key for application"
        },
        "accessToken": {
            "type": "apiKey",
            "name": "x-access-token",
            "in": "header",
            "description": "Unique user authentication token"
        }
    },
    "definitions": {
        "User": {
            "type": "object",
            "properties": {
                "uuid": {
                    "type": "string"
                },
                "first_name": {
                    "type": "string"
                },
                "last_name": {
                    "type": "string"
                },
                "full_name": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "avatar_url": {
                    "type": "string"
                },
                "timezone": {
                    "type": "string"
                },
                "access_token": {
                    "type": "string"
                }
            }
        },
        "Book": {
            "type": "object"
        },
        "Device": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "integer"
                },
                "device_id": {
                    "type": "string"
                },
                "device_type": {
                    "type": "string"
                },
                "device_push_token": {
                    "type": "string"
                },
                "user_id": {
                    "type": "integer"
                },
                "access_token": {
                    "type": "string"
                },
                "access_token_expires_at": {
                    "type": "string"
                },
                "latest_ip_address": {
                    "type": "string"
                },
                "created_at": {
                    "type": "string"
                },
                "updated_at": {
                    "type": "string"
                }
            }
        },
        "File": {
            "type": "object",
            "properties": {
                "uuid": {
                    "type": "string"
                },
                "key": {
                    "type": "string"
                },
                "original_filename": {
                    "type": "string"
                },
                "public_url": {
                    "type": "string"
                },
                "permalink": {
                    "type": "string"
                }
            }
        },
        "SuccessResponse": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean",
                    "default": true
                },
                "payload": {
                    "type": "object"
                }
            }
        },
        "Paginator": {
            "type": "object",
            "properties": {
                "current_page": {
                    "type": "number"
                },
                "per_page": {
                    "type": "number",
                    "default": 50
                },
                "from": {
                    "type": "number"
                },
                "to": {
                    "type": "number"
                },
                "total": {
                    "type": "number"
                },
                "last_page": {
                    "type": "number"
                }
            }
        },
        "ApiErrorUnauthorized": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean",
                    "default": true
                },
                "payload": {
                    "type": "object"
                }
            }
        },
        "ApiErrorAccessDenied": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean",
                    "default": true
                },
                "payload": {
                    "type": "object"
                }
            }
        },
        "ApiError": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean",
                    "default": true
                },
                "payload": {
                    "type": "object"
                }
            }
        },
        "BooksBookListResponse": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean",
                    "default": true
                },
                "payload": {
                    "$ref": "#\/definitions\/Book"
                }
            }
        },
        "BooksShowBookResponse": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "result": {
                    "type": "boolean",
                    "default": true
                },
                "payload": {
                    "$ref": "#\/definitions\/Book"
                }
            }
        }
    }
}