<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\V1\APIBaseController;
use App\Entities\Books\BooksRepository;
use App\Entities\Books\Book;
use EMedia\Api\Docs\APICall;
use EMedia\Api\Docs\Param;
use Illuminate\Http\Request;

class BooksAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(BooksRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
                	return (new APICall())
						->setGroup('Books')
                	    ->setName('Book List')
						->setDescription('Get Book List')
						->setParams([
							(new Param('name', 'String', 'Book name', Param::LOCATION_QUERY)),
							(new Param('sort_order', 'Integer', '`1` => Oldest First, `2` => Newest First', Param::LOCATION_QUERY)),
						])
                        ->setSuccessObject(Book::class);
                });

		$items = $this->repo->search($request->input('name'));
		if(!empty($request->sort_order)){
			switch ($request->sort_order) {
				case Book::SORT_OLDEST:
					$items = $items->sortBy('published_year');
					break;
				case Book::SORT_NEWEST:
					$items = $items->sortByDesc('published_year');
					break;
				default:
					break;
			}
		} 

		return response()->apiSuccess($items, 'Books List');
	}
	
	protected function show($id)
	{
		document(function () {
                	return (new APICall())
                	    ->setGroup('Books')
                	    ->setName('Show Book')
						->setDescription('Get Book Details')
                        ->setSuccessObject(Book::class);
                });
		return response()->apiSuccess(Book::findOrFail($id), 'Books Details');
	}

}
